# Pembagian Kelompok Fortafaste
Project ini dibuat untuk membantu kawan-kawan panitia fortafaste dalam hal pembagian kelompok.
untuk pembelajaran saya belajar membuat uinya dengan vue.js.
project ini akan menyelesaikan tampilan dari algoritma ujicoba ini:
```
//user nanti harusnya bisa masukin nama dari ui
var tif = ['tif idris','tif akbar ','tif adyusman ','tif annie ','tif mikasa '];
var te = ['te tolaal ','te badri ','te alaina ','te eren ','te armin '];
var mt = ['mt ressi ','mt arian ','mt tifa ','mt erwin ','mt connie ','mt historia '];
var sif = ['sif pujo','sif aklita', 'sif made', 'sif jean', 'sif marco'];
var ti = ['ti fadli','ti eric','ti bertolt','ti reiner'];

var jurusan= [tif,te,mt,sif,ti];

//jumlah klompok user yg tentukan
//harus buat cara untuk itu, sementara begini dulu
var kel=[[],[],[],[],[]];

var x = 0;
for(i = 0; i<jurusan.length;i++){
    var j = 0;
    while(j<jurusan[i].length){
        kel[x].push(jurusan[i][j]);
        j++;
        x++;
        if(x==jurusan.length){
            x=0;
        }
    }
}
//last save data kelompok ke database atau download dalam format excel
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
